package com.bonilla.bottombar;

import static android.content.ContentValues.TAG;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.nfc.Tag;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.FrameLayout;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationBarView;
import com.google.android.material.tabs.TabLayout;

public class MainActivity extends AppCompatActivity {

    FrameLayout contenedorFragmentos;
    BottomNavigationView menuInferior;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        menuInferior =findViewById(R.id.menuInferior);
        menuInferior.setOnItemSelectedListener(
                new NavigationBarView.OnItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        int  id=item.getItemId();
                        Log.d(TAG,String.valueOf(id));

                        switch (id){

                            case R.id.iCalc:

                                FragmentManager miAdministrador;
                                FragmentTransaction miTransaccion;
                                miAdministrador = getSupportFragmentManager();
                                miTransaccion = miAdministrador.beginTransaction();
                                miTransaccion.replace(R.id.contenedorFragmentos,new FragmentoCalculadora());
                                miTransaccion.commit();
                             break;

                            case R.id.iTeams:
                                getSupportFragmentManager().beginTransaction().replace(R.id.contenedorFragmentos,new BlankFragment()).commit();

                                break;

                            case R.id.iMap:
                                getSupportFragmentManager().beginTransaction().replace(R.id.contenedorFragmentos,new BlankFragment2()).commit();
                                break;

                        }

                        return false;
                    }
                }

        );

    }
}